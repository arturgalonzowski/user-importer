"""
▀████▄     ▄███▀     ██     ▀███▄   ▀███▀███▀   ▀███▀     ██     ▀████▀
  ████    ████      ▄██▄      ███▄    █  ██       █      ▄██▄      ██
  █ ██   ▄█ ██     ▄█▀██▄     █ ███   █  ██       █     ▄█▀██▄     ██
  █  ██  █▀ ██    ▄█  ▀██     █  ▀██▄ █  ██       █    ▄█  ▀██     ██
  █  ██▄█▀  ██    ████████    █   ▀██▄█  ██       █    ████████    ██     ▄
  █  ▀██▀   ██   █▀      ██   █     ███  ██▄     ▄█   █▀      ██   ██    ▄█
▄███▄ ▀▀  ▄████▄███▄   ▄████▄███▄    ██   ▀██████▀▀ ▄███▄   ▄████▄█████████
"""


class ui_manual:

    def __init__(self):
        self.__user_data = []
        self.__username = None
        self.__password = None
        return

    # Again, simple, yet effective
    # I am doing this only for the sake of continuity
    def input(self):
        self.__username = input("Username: ")
        self.__password = input("Password: ")
        self.__user_data.append((self.__username, self.__password))
        return

    def output(self):
        return self.__user_data
