"""
      ██     ▀███▀▀▀██▄
     ▄██▄      ██    ▀██▄
    ▄█▀██▄     ██     ▀██
   ▄█  ▀██     ██      ██
   ████████    ██     ▄██
  █▀      ██   ██    ▄██▀
▄███▄   ▄████▄████████▀
"""

from pyad import *
import re
import getpass

class ui_ad:

    def __init__(self, user_data):
        self.__user_data = user_data
        self.__user = None
        self.__query = None
        self.__ou = None
        self.__new_user = None
        self.__password = None
        self.__username = None
        return

    # Authorize with the AD
    def connect(self, server, username):
        # Ask nicely :)

        # username[3:username.find(',')]
        # leave first 3 character till ",", a neat way of extracting cn from whole ou path
        self.__username = username[3:username.find(',')]
        self.__password = getpass.getpass("AD Password for \"" + self.__username + "\": ")
        pyad.set_defaults(ldap_server=server, username=self.__username, password=self.__password)
        self.__user = pyad.aduser.ADUser.from_dn(username)
        print("Connected to: ", server)
        return

    # Selecting correct OU
    def select(self):
        # a nice interactive game like in DB, if not possible, use cfg then or type manually...
        return

    # Create user
    def create(self, ou):
        # "Choose" directory
        self.__ou = pyad.adcontainer.ADContainer.from_dn(ou)

        # For each user, create user
        for index in self.__user_data:
            print("Creating: " + index[0] + ", " + re.sub(".", "*", index[1]) + " in: " + ou)
            self.__new_user = pyad.aduser.ADUser.create(index[0], self.__ou, password=index[1], enable=True)
        return
