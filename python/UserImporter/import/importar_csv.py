"""
  ▄▄█▀▀▀█▄█▄█▀▀▀█▄█████▀   ▀███▀
▄██▀     ▀███    ▀█ ▀██     ▄█
██▀       ▀███▄      ██▄   ▄█
██          ▀█████▄   ██▄  █▀
██▄       ▄     ▀██   ▀██ █▀
▀██▄     ▄▀█     ██    ▄██▄
  ▀▀█████▀█▀█████▀      ██
"""

import pandas


class ui_csv:

    def __init__(self):
        self.__user_data = []
        self.__data = None
        return

    def input(self, path):
        # Simple, yet effective
        self.__data = pandas.DataFrame(pandas.read_csv(path), columns=['username', 'name', 'surname', 'password'])

        # Convert to tuple list
        self.__data = list(self.__data.itertuples(index=False, name=None))
        for value in self.__data:
            # leave only 1st and last column
            value = (value[0], value[-1])
            self.__user_data.append(value)
        return

    def output(self):
        return self.__user_data