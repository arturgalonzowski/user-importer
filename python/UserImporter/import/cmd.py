# cmd.py
# All kinds of outputs for command line

# Help text, I like stuff organized
def ui_help():
    print("User Importer v0.1b\n"
          "Author: Artur Galonzowski\n"
          "\n"
          "Usage: \n"
          "ui [var]\n"
          "\n"
          "Arguments:\n"
          "\t-h\tPrints this help\n"
          "\t-m\tInput the data manually\n"
          "\t-c\tImports from CSV file\n"
          "\t-d\tImport from database\n"
          "\t-\t\n")
    return
