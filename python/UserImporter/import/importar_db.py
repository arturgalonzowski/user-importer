"""
▀███▀▀▀██▄ ▀███▀▀▀██▄
 ██    ▀██▄ ██    ██
 ██     ▀██ ██    ██
 ██      ██ ██▀▀▀█▄▄
 ██     ▄██ ██    ▀█
 ██    ▄██▀ ██    ▄█
▄████████▀ ▄████████
"""

import mariadb
import getpass

# Maria db is pretty nice
# It gives you commandline-like experience
# The results are in tuples

class ui_db:

    def __init__(self):
        self.__user_data = []
        self.__database = ""
        self.__databases = []
        self.__table = ""
        self.__tables = []
        self.__columns_data = []
        self.__columns_number = []
        self.__connection = None
        self.__cursor = None
        self.__row = None
        self.__password = None
        return

    def connect(self, server, username):
        # Ask nicely :)
        self.__password = getpass.getpass("DB Password for \"" + username + "\": ")
        # and auth with the server
        try:
            self.__connection = mariadb.connect(
                user=username,
                password=self.__password,
                host=server,
                port=3306
            )
        except mariadb.Error as e:
            exit(f"Error connecting to MariaDB: {e}")
        print("Connected with: ", server)
        return

    def select_database(self):
        self.__cursor = self.__connection.cursor()
        self.__cursor.execute("SHOW DATABASES;")
        self.__row = self.__cursor.fetchall()

        # Convert tuples to string, so I can compare
        for item in self.__row:
            self.__databases.extend(item)

        # A nice sorting-selecting stuff
        print("Select database: ")
        for count, value in enumerate(self.__databases):
            print("    ", count, value)
        self.__database = self.__databases[int(input())]
        print("\nYou have selected: ", self.__database)
        self.__cursor.execute("USE " + self.__database + ";")
        return

    def select_table(self):
        # Similar as in select_database
        self.__cursor.execute("SHOW TABLES;")
        self.__row = self.__cursor.fetchall()
        for item in self.__row:
            self.__tables.extend(item)
        print("Select Table: ")
        for count, value in enumerate(self.__tables):
            print("    ", count, value)
        self.__table = self.__tables[int(input())]
        print("\nYou have selected: ", self.__table)
        return

    # Select users
    def acquire(self):
        # Tell me which are needed
        self.__cursor.execute("DESCRIBE " + self.__table + ";")
        self.__row = self.__cursor.fetchall()

        # Interesting, so you have row[x][y], where
        # x is the column itself, and y is its attributes
        for item in self.__row:
            for name in item:
                self.__columns_data.append(name)
                break
        print("Select username, and password (ex. 1,3): ")

        for count, value in enumerate(self.__columns_data):
            print("    ", count, value)

        self.__columns_number = input().split(',')
        if not self.__columns_number[0] or not self.__columns_number[1]:
            self.__cursor.close()
            self.__connection.close()
            exit("Please, username & password are the minimum!")
        print("You have selected: ",
              self.__columns_data[int(self.__columns_number[0])] + ", " +
              self.__columns_data[int(self.__columns_number[1])]
              )

        # Receive data
        self.__cursor.execute(
            "SELECT " +
            self.__columns_data[int(self.__columns_number[0])] + "," +
            self.__columns_data[int(self.__columns_number[1])] + " FROM " + self.__table + ";")
        self.__user_data = self.__cursor.fetchall()
        return

    def disconnect(self):
        self.__cursor.close()
        self.__connection.close()
        return

    def output(self):
        return self.__user_data