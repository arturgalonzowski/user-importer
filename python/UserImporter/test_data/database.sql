CREATE DATABASE company;
USE company;
create table users(
   id INT NOT NULL AUTO_INCREMENT,
   username VARCHAR(100) NOT NULL,
	vorname VARCHAR(100),
  	nachname VARCHAR(100),
   geschlecht VARCHAR(1),
	geburtsdatum DATE,
   passwort VARCHAR(127),
   PRIMARY KEY ( id )
);
INSERT INTO users (username, vorname, nachname, geschlecht, geburtsdatum, passwort) VALUES
('jtuwim','Julian','Tuwim','m','1894-09-13','passw0rt'),
('jwick','John','Wick','m','2014-10-13','J0Hn_Wick'),
('slem','Stanislaw','Lem','m','1921-09-21','To-The-Stars!1'),
('sconnor','Sarah','Connor','f','1984-10-26','N0RobotsA11owed!')
