# Python imports
import sys
import os
import configparser

# My imports
sys.path.insert(1, 'import')
import importar_manualmente
import importar_csv
import importar_db
import connector_ad
import cmd


# Is config there?
if not os.path.isfile('ui.cfg'):
    print("Config not Found!")
    exit(1)

# init the classes
config = configparser.ConfigParser()
# This is a joke, it worked, but now it didn't, and now for some reason it works
config.read(os.path.join(os.path.abspath(os.path.dirname(__file__)), 'ui.cfg'))


# Check for Arguments
if len(sys.argv) <= 1 or "-h" in sys.argv:
    cmd.ui_help()
    exit(0)

# Manual
if "-m" in sys.argv:
    manual = importar_manualmente.ui_manual()
    manual.input()
    AD = connector_ad.ui_ad(manual.output())

# Database
elif "-d" in sys.argv:
    database = importar_db.ui_db()
    database.connect(
        config.get('database', 'server'),
        config.get('database', 'username')
    )
    database.select_database()
    database.select_table()
    database.acquire()
    AD = connector_ad.ui_ad(database.output())
    database.disconnect()

# CSV
elif "-c" in sys.argv:
    csv = importar_csv.ui_csv()
    csv.input(config.get('csv', 'path'))
    AD = connector_ad.ui_ad(csv.output())
else:
    exit("Please specify valid arguments!")


# Connecting with AD
AD.connect(
    config.get('ldap', 'server'),
    config.get('ldap', 'username')
)

# Creating Users
AD.create(config.get('ldap', 'base_ou'))

exit(0)
